import java.util.*;
import java.io.*;

public class RankTable {
    // table row data
    public class TeamStats {
        private int wins, losses, draws, gf, ga, rank;
        private String medal, team;

        public TeamStats(String team) {
            this.team = team;
            rank = 0; medal = "";
            wins = 0; losses = 0; draws = 0;
            gf = 0; ga = 0;
        }

        public int compareTo(TeamStats s) {
            int p1 = getPoints();
            int p2 = s.getPoints();
            if (p1 > p2) {
                return 1;
            } else if (p1 < p2) {
                return -1;
            } else {
                Integer gd1 = getGD();
                Integer gd2 = s.getGD();
                return gd1.compareTo(gd2);
            }
        }

        @Override
        public String toString() {
            return String.format("%s %d %d %d %d %2d %2d %2d %3d %s",
                team, rank, wins, losses, draws, gf, ga,
                getPoints(), getGD(), medal);
        }

        public String getTeam() {return team;}
        public int getWins() {return wins;}
        public int getDraws() {return draws;}
        public int getLosses() {return losses;}
        public int getGF() {return gf;}
        public int getGA() {return ga;}

        public int getRank() {return rank;}
        public void setRank(int x) {rank = x;}

        public int getPoints() { return 3*wins + draws;}
        public int getGD() { return gf - ga;}

        public String getMedal() { return medal;}
        public void setMedal(String x) { medal = x;}
    }

    private static HashMap<Integer,String> medalsMap = 
        new HashMap<Integer,String>() {{
            put(1,"Gold"); put(2,"Silver"); put(3,"Bronze"); put(4,"Potato");
        }};

    private HashMap<String,Integer> idxMap;
    private ArrayList<TeamStats> teamsStats;

    @Override
    public String toString() {
        String buf = new String("team R W L D GF GA PS +/- Medal\n");
        for (TeamStats a: teamsStats) {
            buf += a.toString() + "\n"; 
        }
        return buf;
    }

    public void updateStats(Match m) {
        String team1 = m.getTeam1();
        String team2 = m.getTeam2();
        int gs1 = m.getGs1();
        int gs2 = m.getGs2();

        // get row index
        int idx1 = idxMap.get(team1);
        int idx2 = idxMap.get(team2);
        TeamStats s1 = teamsStats.get(idx1);
        TeamStats s2 = teamsStats.get(idx2);

        // GA, GF
        s1.gf += gs1; s2.ga += gs1;
        s2.gf += gs2; s1.ga += gs1;
        // // Win, Loss, Draw
        if (gs1 > gs2) { s1.wins++; s2.losses++;}
        else if (gs1 < gs2) { s1.losses++; s2.wins++;}
        else { s1.draws++; s2.draws++;}

        teamsStats.set(idx1, s1);
        teamsStats.set(idx2, s2);
    }

    public void rank() {
        teamsStats.sort((s1,s2) -> -s1.compareTo(s2));
        idxMap = null;

        int rank = 0; int inc = 1;
        int idx = 0; int medalRank = 0;
        TeamStats prev = null;

        for (TeamStats s: teamsStats) {
            if (prev != null && prev.compareTo(s) == 0) {
                // same score -> same rank
                inc++;
            } else {
                // lower score
                medalRank++;
                rank += inc;
                inc = 1;
            }

            s.setMedal(medalsMap.getOrDefault(medalRank, ""));
            s.setRank(rank);
            teamsStats.set(idx++, s);
            prev = s;
        }
    }

    public void save(BufferedWriter writer) throws IOException {
        writer.write(toString());
    }

    public RankTable(ArrayList<String> teamList) {
        teamsStats = new ArrayList<>();
        idxMap = new HashMap<>();
        int i = 0;
        for (String team: teamList) {
            idxMap.put(team, i++);
            teamsStats.add(new TeamStats(team));
        }
    }

    public ArrayList<TeamStats> getData() {
        return teamsStats;
    }
}
