// 16:04 finished, ~600 loc < 26h almost no experience with java before

import java.util.*;
import java.io.*;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.layout.HBox;
import javafx.scene.layout.AnchorPane;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogPane;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.geometry.Insets;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TableRow;

public class JavaBall extends Application {

    private static String teamsInFname = "TeamsIn.txt";
    private static String resultsInFname = "ResultsIn.txt";
    private static String resultsOutFname = "ResultsOut.txt";

    private static Stage primaryStage;

    private void setPrimaryStage(Stage stage) {
        primaryStage = stage;
    }

    // backend data
    private ArrayList<String> teamList;
    private ArrayList<String> withdrawnList;
    private ArrayList<Match> matchesList;
    private RankTable rankTable;
    private TableView<Match> table;
    private TableView<RankTable.TeamStats> rankTableView;
    private boolean gameFinished;

    // widgets
    Button resultsInBtn, withdrawBtn, addBtn, processBtn, exitBtn;

    public JavaBall() {
        super();
        teamList = new ArrayList<>();
        withdrawnList = new ArrayList<>();
        matchesList = new ArrayList<>();
        rankTable = null;
        table = new TableView<>();
        rankTableView = new TableView<>();
        gameFinished = false;
    }

    private void loadTeams(String filename) {
        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
            String line;
            while ((line = br.readLine()) != null) {
               teamList.add(line);
            }
        } catch (Exception e) {
            showDialog("File not found: " + filename, AlertType.ERROR,
                "Application Error", "Error");
        }
        assert(teamList.size() >= 4 && teamList.size() <= 8);
        Collections.sort(teamList); 
    }

    private void initializeMatches() {
        int n = teamList.size();
        for (int i = 0; i < n; i++)
            for (int j = i + 1; j < n; j++)
                matchesList.add(new Match(teamList.get(i), teamList.get(j)));
    }

    private void showDialog(String msg, Alert.AlertType type, String title,
        String header)
    {
        Alert alert = new Alert(type);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(msg);
        alert.showAndWait();
        if (type == AlertType.ERROR) {
            Platform.exit();
        }
    }

    private void loadMatches(String filename) {
        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] tokens = line.split("[ ]+");
                if (tokens.length == 4) {
                    String team1 = tokens[0];
                    int gs1 = Integer.parseInt(tokens[1]);
                    String team2 = tokens[2];
                    int gs2 = Integer.parseInt(tokens[3]);

                    // check input
                    assert(gs1 >= 0 && gs2 >= 0 && gs1 <= 9 && gs2 <= 9);
                    assert(!team1.equals(team2));
                    assert(teamList.contains(team1) ||
                        withdrawnList.contains(team1));

                    assert(teamList.contains(team2) ||
                        withdrawnList.contains(team2));

                    // only active teams
                    if (teamList.contains(team1) && teamList.contains(team2))
                        addMatch(team1, team2, gs1, gs2);
                } else {
                    throw new Exception();
                }
             }
        } catch (Exception e) {
            showDialog("Error while loading " + filename, AlertType.ERROR,
                "Application Error", "Error");
        }

        table.refresh();
    }

    private void addMatch(String team1, String team2, int gs1, int gs2) {
        // change the order
        if (team1.compareTo(team2) > 0) {
            String tmp1 = team1; team1 = team2; team2 = tmp1;
            int tmp2 = gs1; gs1 = gs2; gs2 = tmp2;
        }
        
        // System.out.println("add match");
        boolean notFound = true;
        int completed = 0;
        for (int i = 0; i < matchesList.size(); i++) {
            Match m = matchesList.get(i);
            // if not set
            if (m.tbd()) {
                // System.out.println("tbd");
                if (m.getTeam1().equals(team1) && m.getTeam2().equals(team2)) {
                    // System.out.println("matched");
                    m.setResult(gs1, gs2);
                    matchesList.set(i, m);
                    completed++;
                    notFound = false;
                }
            } else {
                completed++;
            }
        }

        if (notFound) {
            showDialog("invalid result data", AlertType.WARNING, "Warning",
                "Warning");
        }

        // can't add more results
        if (completed == matchesList.size()) {
            gameFinished = true;
            addBtn.setDisable(true);
            processBtn.setDisable(false);
        }
    }

    private void withdrawTeam() {
        ChoiceDialog<String> dialog = new ChoiceDialog<>(teamList.get(0),
            teamList);
        dialog.setTitle("Withdraw a Team");
        dialog.setHeaderText("Removing the Team from the Tournament");
        dialog.setContentText("Please choose the team name:");

        // Traditional way to get the response value.
        Optional<String> result = dialog.showAndWait();
        if (!result.isPresent()) {
            return;
        }

        String team = result.get();
        teamList.remove(team);

        // exit if # of teams is not sufficient
        if (teamList.size() < 3) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Tournament Info");
            alert.setContentText("The tournament has been canceled");

            alert.showAndWait();
            Platform.exit();
            return;
        }

        // remove the team from match list
        matchesList.removeIf(m -> m.getTeam1().equals(team) ||
            m.getTeam2().equals(team));
        withdrawnList.add(team);
        
        // re-render table
        table.getItems().clear();
        table.setItems(getMatches());
    }

    private void addBtnClicked() {
        addBtnClicked("team 1", "team 2");
    }

    private void addBtnClicked(String team1name, String team2name) {
        Dialog<Match> dialog = new Dialog<>();
        dialog.setTitle("Add Match");
        dialog.setHeaderText("Please specify the match result");
        DialogPane dialogPane = dialog.getDialogPane();
        dialogPane.getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
        // team names
        TextField team1 = new TextField(team1name);
        TextField team2 = new TextField(team2name);

        // goals scored
        ObservableList<Integer> options = FXCollections.observableArrayList(
            new Integer[]{0,1,2,3,4,5,6,7,8,9});
        ComboBox<Integer> gs1 = new ComboBox<>(options);
        gs1.getSelectionModel().selectFirst();
        ComboBox<Integer> gs2 = new ComboBox<>(options);
        gs2.getSelectionModel().selectFirst();

        GridPane grid = new GridPane();
        grid.setHgap(10); grid.setVgap(10);
        grid.setPadding(new Insets(20,100,10,10));
        grid.add(new Label("Team 1:"), 0, 0);
        grid.add(team1, 1, 0);
        grid.add(new Label("Team 2:"), 0, 1);
        grid.add(team2, 1, 1);
        
        grid.add(new Label("Goals Scored Team 1:"), 0, 2);
        grid.add(gs1, 1, 2);
        grid.add(new Label("Goals Scored Team 2:"), 0, 3);
        grid.add(gs2, 1, 3);

        dialogPane.setContent(grid);
        Platform.runLater(team1::requestFocus);

        dialog.setResultConverter((ButtonType button) -> {
            if (button == ButtonType.OK) {
                return new Match(team1.getText(), team2.getText(),
                    gs1.getValue(), gs2.getValue());
            }
            return null;
        });

        Optional<Match> optionalResult = dialog.showAndWait();
        optionalResult.ifPresent((Match result) -> {
            addMatch(result.getTeam1(), result.getTeam2(), result.getGs1(),
                result.getGs2());
            table.refresh();
        });
    }

    private void processBtnClicked() {
        rankTable = new RankTable(teamList);
        for (Match m: matchesList) {
            rankTable.updateStats(m);
        }
        rankTable.rank();
        processBtn.setDisable(true);
        setScene();
        // System.out.println(rankTable);
    }

    private void saveAndExit() {
        try {
            BufferedWriter writer = new BufferedWriter(
                new FileWriter(resultsOutFname));

            if (gameFinished) {
                rankTable.save(writer);
            } else {
                for (Match m: matchesList) {
                    if (!m.tbd()) {
                        String buf = String.format("%s %d %s %d\n", 
                            m.getTeam1(), m.getGs1(), m.getTeam2(), m.getGs2());
                        writer.write(buf);
                    }
                }
            }

            writer.close();
        } catch (Exception e) {
            showDialog("Error while saving result", AlertType.ERROR,
                "Application Error", "Error");
        }
        Platform.exit();
    }

    public static void main(String args[]) {
        launch(args);
    }

    public ObservableList<Match> getMatches() {
        ObservableList<Match> ret = FXCollections.observableArrayList();
        ret.addAll(matchesList);
        return ret;
    }

    public ObservableList<RankTable.TeamStats> getTeamsStats() {
        ObservableList<RankTable.TeamStats> ret = 
            FXCollections.observableArrayList();
        ret.addAll(rankTable.getData());
        return ret;
    }

    private void setTable() {
        if (gameFinished) {
            rankTableView.setMaxWidth(Double.MAX_VALUE);
            String colNames[] = {"team", "rank", "wins", "draws", "losses",
                "GF", "GA", "points", "GD", "medal"};
            int colWidths[] = {150, 40, 40, 40, 40,
                               40, 40, 40, 40, 120};
            for (int i = 0; i < colNames.length; i++) {
                String str = colNames[i];
                TableColumn<RankTable.TeamStats,String> col = 
                    new TableColumn<>(str);
                col.setCellValueFactory(new PropertyValueFactory<>(str));
                col.setMinWidth(colWidths[i]);
                rankTableView.getColumns().add(col);
            }
            rankTableView.setItems(getTeamsStats());
            // remove extra empty column
            /* https://stackoverflow.com/questions/12933918/
               tableview-has-more-columns-than-specified
            */
            rankTableView.setColumnResizePolicy(
                TableView.CONSTRAINED_RESIZE_POLICY);
        } else {
            table.setMaxWidth(Double.MAX_VALUE);
            String arr[] = {"team1", "team2", "result"};
            for (String s: arr) {
                TableColumn<Match,String> col = new TableColumn<>(s);
                col.setCellValueFactory(new PropertyValueFactory<>(s));
                col.setMinWidth(170);
                table.getColumns().add(col);
            }
            // remove extra empty column
            table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

            // edit on double click
            /* https://stackoverflow.com/questions/26563390/
               detect-doubleclick-on-row-of-tableview-javafx
            */
            table.setRowFactory( tv -> {
                TableRow<Match> row = new TableRow<>();
                row.setOnMouseClicked(event -> {
                    if (!addBtn.isDisable() && event.getClickCount() == 2 &&
                        (!row.isEmpty()))
                    {
                        Match m = row.getItem();
                        if (m.tbd()) {
                            addBtnClicked(m.getTeam1(), m.getTeam2());
                        }
                    }
                });
                return row ;
            });
        }
    }

    private void setScene() {
        // setting table view and layout
        VBox vbox = new VBox();
        vbox.setSpacing(10);
        vbox.setPadding(new Insets(10, 40, 20, 40)); 
        vbox.getChildren().addAll(resultsInBtn, withdrawBtn, addBtn, processBtn,
            exitBtn);

        // apply to all buttons
        for (Object o: vbox.getChildren()) {
            ((Button) o).setMaxWidth(Double.MAX_VALUE);      
            ((Button) o).setMinWidth(100);
        }
        
        setTable();
        HBox layout = new HBox();
        if (gameFinished)
            layout.getChildren().addAll(rankTableView, vbox);
        else
            layout.getChildren().addAll(table, vbox);
        
        // setting scene and layout
        Scene scene = new Scene(layout);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    @Override
    public void start(Stage stage) throws Exception {
        setPrimaryStage(stage);
        // change close button
        primaryStage.setOnCloseRequest(event -> {
            // consume event
            event.consume();

            // show close dialog
            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.setTitle("Save & Exit Confirmation");
            alert.setHeaderText("Cannot close the application without saving.");
            alert.setContentText("Do you wish to save & exit?");
            alert.initOwner(primaryStage);

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK){
                saveAndExit();
            }
        });

        primaryStage.setTitle("Java Ball");

        // initialize buttons and define actions on events
        withdrawBtn = new Button("withdraw team");
        withdrawBtn.setOnAction(e -> {withdrawTeam();});

        addBtn = new Button("add match");
        addBtn.setDisable(true);
        addBtn.setOnAction(e -> {addBtnClicked();});

        resultsInBtn = new Button("load results");
        resultsInBtn.setOnAction(e -> {
            resultsInBtn.setDisable(true);
            withdrawBtn.setDisable(true);
            loadMatches(resultsInFname);
            // set disabled when finished
            addBtn.setDisable(gameFinished);
        });

        processBtn = new Button("process results");
        processBtn.setDisable(true);
        processBtn.setOnAction(e -> {processBtnClicked();});

        exitBtn = new Button("save & exit");
        exitBtn.setOnAction(e -> {saveAndExit();});

        setScene();
        
        loadTeams(teamsInFname);        // read data from file
        initializeMatches();
        table.setItems(getMatches());   // populate table
    }

}