// Match data model
public class Match {
    private String team1, team2;
    private int gs1, gs2;

    public Match(String team1, String team2) {
        this.team1 = team1;
        this.team2 = team2;
        this.gs1 = -1;
        this.gs2 = -1;
    }

    public Match(String team1, String team2, int gs1, int gs2) {
        this.team1 = team1;
        this.team2 = team2;
        this.gs1 = gs1;
        this.gs2 = gs2;
    }

    public boolean tbd() { return gs1 == -1;}

    public String getTeam1() { return this.team1;}
    public void setTeam1(String team1) { this.team1 = team1;}

    public String getTeam2() { return this.team2; }
    public void setTeam2(String team2) { this.team2 = team2;}

    public int getGs1() { return this.gs1;}
    public int getGs2() { return this.gs2;}

    public String getResult() {
        if (gs1 == -1)
            return "** TBD **";
        else
            return String.format("%s : %s", gs1, gs2);
    }
    public void setResult(int gs1, int gs2) {
        this.gs1 = gs1;
        this.gs2 = gs2;
    }
}